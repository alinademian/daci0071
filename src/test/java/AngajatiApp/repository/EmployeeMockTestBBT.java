package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeeMockTestBBT {

    EmployeeMock repo = new EmployeeMock();

    @Test
    public void addEmployee1() {

        Employee e = new Employee("Ana", "Pop", "1748574021469", DidacticFunction.ASISTENT, 1000d);

        assertTrue(repo.addEmployee(e));
    }

    @Test
    public void addEmployee2() {

        Employee e = new Employee("An", "Bob", "274153641257", DidacticFunction.PROFESOR, 1000d);

        assertFalse(repo.addEmployee(e));

    }

    @Test
    public void addEmployee3() {

        Employee e = new Employee("Numecudouazecilitere", "Numedouazecisicincilitere", "1746524013214", DidacticFunction.CONFERENTIAR, 8000d);

        assertTrue(repo.addEmployee(e));
    }

    @Test
    public void addEmployee4() {

        Employee e = new Employee("Aa", "Bb", "23656478547251", DidacticFunction.CONFERENTIAR, 1000d);

        assertFalse(repo.addEmployee(e));

    }

    @Test
    public void addEmployee5() {

        Employee e = new Employee("Ab", "Bc", "23656478547251", DidacticFunction.PROFESOR, 999d);

        assertFalse(repo.addEmployee(e));
    }

    @Test
    public void addEmployee6() {

        Employee e = new Employee("Ada", "Mar", "145749602145", DidacticFunction.LECTURER, 8001d);

        assertFalse(repo.addEmployee(e));
    }

    @Test
    public void addEmployee7() {

        Employee e = new Employee("Aba", "Ben", "2417596021485", DidacticFunction.CONFERENTIAR, 8000d);

        assertTrue(repo.addEmployee(e));
    }

    @Test
    public void addEmployee8() {

        Employee e = new Employee("nhtfrstrfgytrtgtsrtg", "bhtsrtrtgtgbfsrtasertrurt", "1576341950212", DidacticFunction.ASISTENT, 1000d);

        assertTrue(repo.addEmployee(e));
    }
}